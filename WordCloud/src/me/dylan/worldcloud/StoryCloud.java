package me.dylan.worldcloud;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class StoryCloud {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Word Cloud");
		frame.setSize(500, 350);
		frame.setLocationRelativeTo(null);
		
		WordPanel panel = new WordPanel();
		panel.setSize(500, 350);
		panel.init();
		
		frame.getContentPane().add(panel);

		frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				System.exit(0);
			}
			
		});
		
		frame.setVisible(true);
		
	}
	
}
