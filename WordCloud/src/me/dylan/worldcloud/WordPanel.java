package me.dylan.worldcloud;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import wordcloud.CollisionMode;
import wordcloud.WordCloud;
import wordcloud.WordFrequency;
import wordcloud.bg.CircleBackground;
import wordcloud.font.CloudFont;
import wordcloud.font.scale.SqrtFontScalar;
import wordcloud.nlp.FrequencyAnalyzer;
import wordcloud.palette.ColorPalette;	

@SuppressWarnings("serial")
public class WordPanel extends JPanel {
	
	private JButton open;
	private JButton export;
	private DefaultListModel<String> listModel;
	private JLabel word;
	private JLabel occurances;
	private File pdf;
	private Map<String, Integer> values;
	private JCheckBox insignifigantFilter;
	private JTextField occuranceLimit;
	private JTextField startingPage;
	private JTextField endingPage;
	private JLabel fileName;
	
	private String lastWord = "";
	
	public WordPanel() {
		this.setLayout(null);
	}
	
	public void init() {
		listModel = new DefaultListModel<>();
		JList<String> list = new JList<String>(listModel);
		list.setBackground(new Color(248, 248, 248));
		list.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		
		list.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				if (!(lastWord.equals(list.getSelectedValue()))) {
					lastWord = list.getSelectedValue();
					selectWord(list.getSelectedValue());
				}
			}
			
		});
		
		list.addMouseMotionListener(new MouseAdapter() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				super.mouseMoved(e);
				if (!list.isSelectionEmpty()) {
					if (!(lastWord.equals(list.getSelectedValue()))) {
						lastWord = list.getSelectedValue();
						selectWord(list.getSelectedValue());
					}
				}
			}
			
		});
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setSize(200, 250);
		scrollPane.setLocation(20, 30);
		
		this.add(scrollPane);
		
		open = new JButton("Open");
		open.setLocation(this.getWidth() - 130, 10);
		open.setSize(100, 20);
		
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					pdf = fc.getSelectedFile();
					selectPDF();
				}
			}
			
		});
		
		this.add(open);
		
		export = new JButton("Export");
		export.setLocation(this.getWidth() - 250, 10);
		export.setSize(100, 20);
		
		export.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					PDDocument doc = PDDocument.load(pdf);
					PDFTextStripper reader = new PDFTextStripper();
					int spage = 1;
					int epage = doc.getNumberOfPages();
					if (!startingPage.getText().equals("")) {
						spage = Integer.parseInt(startingPage.getText());
					}
					if (!endingPage.getText().equals("")) {
						epage = Integer.parseInt(endingPage.getText());
					} else {
						endingPage.setText("" + endingPage);
					}
					reader.setStartPage(spage);
					reader.setEndPage(epage);
					String text = reader.getText(doc);
					List<String> wordList = new ArrayList<String>();
					int occurancel = 0;
					if (!occuranceLimit.getText().equals(""))
						occurancel = Integer.parseInt(occuranceLimit.getText());
					for (String word : getWords(text)) {
						if (values.get(word) == null)
							continue;
						if (occurancel != 0) {
							if (!(values.get(word) >= occurancel))
								wordList.add(word);
						} else
							wordList.add(word);
					}
					
					final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
					final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load(wordList);
	
					final WordCloud wordCloud = new WordCloud(600, 600, CollisionMode.PIXEL_PERFECT);
					wordCloud.setPadding(2);
					wordCloud.setBackground(new CircleBackground(150));
					wordCloud.setColorPalette(new ColorPalette(Color.white, Color.red, Color.green, Color.blue, Color.yellow, Color.pink, Color.cyan, Color.gray, Color.magenta, Color.orange));
					wordCloud.setFontScalar(new SqrtFontScalar(10, 40));
					wordCloud.setCloudFont(new CloudFont(new Font(fileName.getFont().getName(), Font.BOLD, 24)));
					wordCloud.build(wordFrequencies);
					
					final JFileChooser fc = new JFileChooser();
					int returnVal = fc.showSaveDialog(null);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = fc.getSelectedFile();
						System.out.println(file.getAbsolutePath());
						if (!file.exists()) file.createNewFile();
						wordCloud.writeToFile(file.getAbsolutePath() + ".png");
						Desktop dt = Desktop.getDesktop();
						dt.open(new File(file.getAbsolutePath() + ".png"));
						file.delete();
					}
					
					JOptionPane.showMessageDialog(null, "Exported selected word cloud!");
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Something went wrong with the export");
					e1.printStackTrace();
				}
			}
			
		});
		
		this.add(export);
		
		word = new JLabel("Word");
		word.setSize(200, 200);
		word.setFont(new Font(word.getFont().getName(), Font.BOLD, 32));
		word.setLocation(310, 100);
		
		this.add(word);
		
		occurances = new JLabel("Occurances: ");
		occurances.setSize(200, 200);
		occurances.setFont(new Font(word.getFont().getName(), Font.ITALIC, 12));
		occurances.setLocation(270, 150);
		
		this.add(occurances);
		
		insignifigantFilter = new JCheckBox("Filter Offensive Words");
		insignifigantFilter.setSize(300, 20);
		insignifigantFilter.setLocation(270, 45);
		
		this.add(insignifigantFilter);
		
		occuranceLimit = new JTextField("");
		occuranceLimit.setSize(40, 20);
		occuranceLimit.setLocation(270, 75);
		
		JLabel occuranceText = new JLabel("Occurance limit");
		occuranceText.setSize(400, 20);
		occuranceText.setLocation(315, 75);
		
		this.add(occuranceLimit);
		this.add(occuranceText);
		
		JLabel startPageText = new JLabel("Starting Page");
		startPageText.setFont(new Font(startPageText.getFont().getName(), 0, 10));
		startPageText.setSize(400, 20);
		startPageText.setLocation(270, 100);
		
		JLabel endPageText = new JLabel("Ending Page");
		endPageText.setFont(new Font(endPageText.getFont().getName(), 0, 10));
		endPageText.setSize(400, 20);
		endPageText.setLocation(360, 100);
		
		this.add(startPageText);
		this.add(endPageText);
		
		startingPage = new JTextField("1");
		startingPage.setSize(30, 20);
		startingPage.setLocation(285, 120);
		
		this.add(startingPage);
		
		endingPage = new JTextField("");
		endingPage.setSize(30, 20);
		endingPage.setLocation(375, 120);
		
		this.add(endingPage);
		
		JButton filter = new JButton("Filter");
		filter.setSize(200, 20);
		filter.setLocation(250, 150);
		
		filter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				selectPDF();
			}
			
		});
		
		this.add(filter);
		
		fileName = new JLabel("PDF: ");
		fileName.setFont(new Font(fileName.getFont().getName(), Font.ITALIC, 10));
		fileName.setSize(400, 20);
		fileName.setLocation(20, 5);
		
		this.add(fileName);
		
	}
	
	private void selectPDF() {
		fileName.setText("PDF: " + pdf.getName());
		try {
			PDDocument doc = PDDocument.load(pdf);

			PDFTextStripper reader = new PDFTextStripper();
			int startingPage = 1;
			int endingPage = doc.getNumberOfPages();
			if (!this.startingPage.getText().equals("")) {
				startingPage = Integer.parseInt(this.startingPage.getText());
			}
			if (!this.endingPage.getText().equals("")) {
				endingPage = Integer.parseInt(this.endingPage.getText());
			} else {
				this.endingPage.setText("" + endingPage);
			}
			reader.setStartPage(startingPage);
			reader.setEndPage(endingPage);
			String text = reader.getText(doc);
			String[] words = getWords(text);
			sortWords(words);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading PDF file...");
			e.printStackTrace();
		}
	}
	
	private void sortWords(String[] words) {
		listModel.clear();
		HashSet<String> wordHash = new HashSet<String>();
		for (String word : words)
			wordHash.add(word.toLowerCase());
		Object[] singleWords = wordHash.toArray();
		HashMap<String, Integer> frequencies = new HashMap<String, Integer>();
		int occurances;
		for (int i = 0; i < singleWords.length; i ++) {
			occurances = 0;
			for (int j = 0; j < words.length; j ++) {
				if (((String) singleWords[i]).equalsIgnoreCase(words[j]))
					occurances ++;
			}
			frequencies.put((String) singleWords[i], occurances);
		}
		values = sortByValue(frequencies);
		ArrayList<String> keys = new ArrayList<String>(values.keySet());
		String word = "";
		int occuranceLimit = 0;
		if (!this.occuranceLimit.getText().equals("")) {
			occuranceLimit = Integer.parseInt(this.occuranceLimit.getText());
		}
        for (int i = keys.size()-1; i >= 0; i--) {
        	word = keys.get(i);
        	if (word.length() > 1) {
	        	if (values.get(word) > 2) {
	        		if (values.get(word) >= occuranceLimit && occuranceLimit != 0)
	        			continue;
	        		if (insignifigantFilter.isSelected()) {
	        			if (word.equalsIgnoreCase("nigger"))
	        				continue;
	        			listModel.addElement(keys.get(i));
	        		} else
	        			listModel.addElement(keys.get(i));
	        	}
        	}
        }
	}
	
	public void selectWord(String word) {
		this.word.setText(word);
		this.occurances.setText("Occurances: " + values.get(word));
	}
	
	private String[] getWords(String text) {
		String newText = text.replace(",", "").replace("\"", "").replace(".", "").replace("!", "").replace(";", "").replace(":", "").replace("?", "").replace("-", " ").replace("--", " ");
		String[] words = newText.split(" ");
		return words;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ) {
		Map<K,V> result = new LinkedHashMap<>();
		Stream <Entry<K,V>> st = map.entrySet().stream();

		st.sorted(Comparator.comparing(e -> e.getValue())).forEachOrdered(e ->result.put(e.getKey(),e.getValue()));

		return result;
	}
	
}
